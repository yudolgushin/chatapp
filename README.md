This is a simple client chat application. The server is available at the following link: https://bitbucket.org/yudolgushin/chatappserver

Used technologies: FCM, Retrofit, SQLite;

>## TODO
* ~~login~~
* ~~signUp~~
* ~~getting all users~~
* ~~getting all messages whit current user and sort by date~~
* ~~geting all chats and sort by date~~
* ~~send message~~
*~~remove message~~
* ~~receiving and saving messages which come from FCM~~
* sync messages via http Api
* setup user profile: displayname, avatar

>## Screenshots

![Alt text](https://bytebucket.org/yudolgushin/chatapp/raw/044e136723413b05e6d893d08429f697b76f9784/screenshots/device-3.png)
![Alt text](https://bytebucket.org/yudolgushin/chatapp/raw/044e136723413b05e6d893d08429f697b76f9784/screenshots/device-2.png)
![Alt text](https://bytebucket.org/yudolgushin/chatapp/raw/044e136723413b05e6d893d08429f697b76f9784/screenshots/device-1.png)
![Alt text](https://bytebucket.org/yudolgushin/chatapp/raw/044e136723413b05e6d893d08429f697b76f9784/screenshots/device-5.png)