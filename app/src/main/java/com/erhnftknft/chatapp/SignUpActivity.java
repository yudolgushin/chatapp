package com.erhnftknft.chatapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.erhnftknft.chatapp.web.requests.LoginRequest;
import com.erhnftknft.chatapp.web.retrofit.RetrofitService;
import com.erhnftknft.chatapp.usersandchats.UsersAndChatsActivity;

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener {
    EditText loginEditText;
    EditText passwordEditeText;
    EditText passwordRepeatPassword;
    Button signUpButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        ImageButton backButton = findViewById(R.id.back_button);
        backButton.setOnClickListener(v -> onBackPressed());
        loginEditText = findViewById(R.id.login_edit_text);
        passwordEditeText = findViewById(R.id.password_edit_text);
        signUpButton = findViewById(R.id.button_sing_up);
        passwordRepeatPassword = findViewById(R.id.repeat_password_edit_text);
        signUpButton.setOnClickListener(this);


    }



    @Override
    public void onClick(View v) {
        if (passwordEditeText.getText().toString().equals(passwordRepeatPassword.getText().toString())) {
            LoginRequest r = new LoginRequest();
            r.login = loginEditText.getText().toString();
            r.password = passwordEditeText.getText().toString();
            RetrofitService.request(a -> a.signUp(r),
                    p -> {
                        Info.getInstance().setTokenAndIdAndLogin(p.value.accessToken, p.value.userId, loginEditText.getText().toString());
                        Intent i = new Intent(SignUpActivity.this, UsersAndChatsActivity.class);
                        startActivity(i);
                    }, t -> {
                        Toast.makeText(SignUpActivity.this, t.toString(), Toast.LENGTH_SHORT).show();
                    });
        } else {
            Toast.makeText(SignUpActivity.this, "passwords are not the same ", Toast.LENGTH_SHORT).show();
        }


    }
}
