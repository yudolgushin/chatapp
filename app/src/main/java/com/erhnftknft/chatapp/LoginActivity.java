package com.erhnftknft.chatapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.erhnftknft.chatapp.usersandchats.UsersAndChatsActivity;
import com.erhnftknft.chatapp.web.requests.LoginRequest;
import com.erhnftknft.chatapp.web.retrofit.RetrofitService;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    Button buttonLogin;
    Button buttonSingUp;
    EditText loginEditText;
    EditText passwordEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        if (Info.getInstance().getAccessToken() != null) {
            goToMain();
        }

        buttonLogin = findViewById(R.id.button_login);
        buttonSingUp = findViewById(R.id.button_sing_up);
        loginEditText = findViewById(R.id.login_edit_text);
        passwordEditText = findViewById(R.id.password_edit_text);
        buttonSingUp.setOnClickListener(this);
        buttonLogin.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_login:
                onLoginButtonClick();
                break;
            case R.id.button_sing_up:
                onSingUpButtonClick();
                break;
        }
    }

    private void onSingUpButtonClick() {
        Intent i = new Intent(this, SignUpActivity.class);
        startActivity(i);
    }

    private void onLoginButtonClick() {
        LoginRequest req = new LoginRequest();
        req.login = loginEditText.getText().toString();
        req.password = passwordEditText.getText().toString();

        RetrofitService.request(a -> a.login(req),
                p -> {
                    Log.d("ACCESS", p.value.accessToken);
                    Info.getInstance().setTokenAndIdAndLogin(p.value.accessToken, p.value.userId, loginEditText.getText().toString());
                    goToMain();
                },
                t -> {
                    Toast.makeText(this, t.toString(), Toast.LENGTH_SHORT).show();

                });
    }

    private void goToMain() {
        Intent i = new Intent(this, UsersAndChatsActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
        finish();
    }


}
