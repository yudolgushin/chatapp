package com.erhnftknft.chatapp.obs;


public interface Observer {
    public void onMessageReceive();
}
