package com.erhnftknft.chatapp.obs;


public interface Observble {
    public void addObserver(Observer observer);

    public void removeObserver(Observer observer);

    public void notifyObserver();
}
