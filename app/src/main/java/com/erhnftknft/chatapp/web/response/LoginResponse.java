package com.erhnftknft.chatapp.web.response;

public class LoginResponse {
    public String accessToken;
    public String userId;

    public LoginResponse(String accessToken) {
        this.accessToken = accessToken;
    }
}
