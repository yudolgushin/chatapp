package com.erhnftknft.chatapp.web.retrofit;


import android.text.TextUtils;
import android.util.Log;

import com.erhnftknft.chatapp.web.requests.RegisterPushTokenRequest;
import com.erhnftknft.chatapp.web.response.BaseResponse;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitService {
    public static final String BASE_URL = "http://192.168.100.9:9090";
    public static final String WEBROOT_IMG = "./webroot/img/";

    //  private static final String BASE_URL = "http://dbrng.sytes.net:9090";
    public static final String LOG_TAG = "RetrofitService";
    ApiService api;
    public static RetrofitService instance;

    public ApiService getApi() {
        return api;
    }

    public RetrofitService() {

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(logging)
                .writeTimeout(40, TimeUnit.SECONDS)
                .connectTimeout(40, TimeUnit.SECONDS)
                .readTimeout(40, TimeUnit.SECONDS)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        api = retrofit.create(ApiService.class);
    }

    public static synchronized RetrofitService getInstance() {
        if (instance == null) {
            instance = new RetrofitService();
        }
        return instance;
    }

    public static <T extends BaseResponse> void request(FunctionRetrofitService<Call<T>, ApiService> callProducer, ActionRetrofitService<T> onSuccess, ActionRetrofitService<Throwable> onFail) {

        Call<T> call = callProducer.call(RetrofitService.getInstance().getApi());


        call.enqueue(new Callback<T>() {
            @Override
            public void onResponse(Call<T> call, Response<T> response) {
                try {
                    T body = response.body();
                    if (BaseResponse.OK.equals(body.result)) {
                        onSuccess.call(body);
                        return;
                    }
                    throw new RuntimeException(body.result + " " + body.errorMessage);
                } catch (Exception e) {
                    e.printStackTrace();
                    onFail.call(e);
                }
            }

            @Override
            public void onFailure(Call<T> call, Throwable t) {
                t.printStackTrace();
                onFail.call(t);
            }
        });
    }

    public static void registerPushToken() {

        String pushToken = FirebaseInstanceId.getInstance().getToken();
        if (TextUtils.isEmpty(pushToken)) {
            return;
        }

        request(param -> param.registerPushToken(new RegisterPushTokenRequest(pushToken)),
                p -> {
                    Log.d(LOG_TAG, "Ok");
                }
                , p -> {
                    p.printStackTrace();
                });

    }
}
