package com.erhnftknft.chatapp.web.retrofit;


import com.erhnftknft.chatapp.models.ChatUser;
import com.erhnftknft.chatapp.models.MessageUser;
import com.erhnftknft.chatapp.models.SyncData;
import com.erhnftknft.chatapp.web.requests.BaseRequest;
import com.erhnftknft.chatapp.web.requests.EditDisplayNameRequest;
import com.erhnftknft.chatapp.web.requests.GetChatUserRequest;
import com.erhnftknft.chatapp.web.requests.LoginRequest;
import com.erhnftknft.chatapp.web.requests.MessageRequest;
import com.erhnftknft.chatapp.web.requests.RegisterPushTokenRequest;
import com.erhnftknft.chatapp.web.response.BaseResponse;
import com.erhnftknft.chatapp.web.response.LoginResponse;

import java.util.List;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface ApiService {
    @POST("/login")
    Call<BaseResponse<LoginResponse>> login(@Body LoginRequest req);

    @POST("/logOut")
    Call<BaseResponse> logOut(@Body GetChatUserRequest req);

    @POST("/getChatUserById")
    Call<BaseResponse<ChatUser>> getChatUserById(@Body GetChatUserRequest req);

    @POST("/signUp")
    Call<BaseResponse<LoginResponse>> signUp(@Body LoginRequest req);

    @POST("/getChatUsers")
    Call<BaseResponse<List<ChatUser>>> getAllUsers(@Body BaseRequest request);

    @POST("/sendMessage")
    Call<BaseResponse<MessageUser>> sendMessage(@Body MessageRequest request);

    @POST("/registerPushToken")
    Call<BaseResponse> registerPushToken(@Body RegisterPushTokenRequest request);

    @POST("/refreshDisplayNameUser")
    Call<BaseResponse> refreshDisplayNameUser(@Body EditDisplayNameRequest request);

    @POST("/sync")
    Call<BaseResponse<SyncData>> sync(@Body BaseRequest request);

    @Multipart
    @POST("/uploadUserAvatar")
    Call<BaseResponse> uploadImgUserAvatar(@Part MultipartBody.Part file, @Part MultipartBody.Part accessToken);

    @Multipart
    @POST("/uploadImg")
    Call<BaseResponse> uploadImg(@Part MultipartBody.Part file, @Part MultipartBody.Part accessToken);

}
