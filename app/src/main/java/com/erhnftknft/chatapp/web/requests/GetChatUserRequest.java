package com.erhnftknft.chatapp.web.requests;


public class GetChatUserRequest {
    public String id;
    public String accessToken;
}
