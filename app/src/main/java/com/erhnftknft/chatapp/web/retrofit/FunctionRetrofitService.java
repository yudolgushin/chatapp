package com.erhnftknft.chatapp.web.retrofit;


public interface FunctionRetrofitService<T, V> {
    T call(V param);
}
