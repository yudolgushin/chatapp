package com.erhnftknft.chatapp.web.requests;

public class RegisterPushTokenRequest extends BaseRequest {
    public String pushToken;

    public RegisterPushTokenRequest(String pushToken) {
        super();
        this.pushToken = pushToken;
    }
}
