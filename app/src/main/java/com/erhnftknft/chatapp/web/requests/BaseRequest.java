package com.erhnftknft.chatapp.web.requests;


import com.erhnftknft.chatapp.Info;

public class BaseRequest {
    public String accessToken;

    public BaseRequest() {
        accessToken = Info.getInstance().getAccessToken();
    }
}