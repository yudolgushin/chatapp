package com.erhnftknft.chatapp.web.retrofit;


public interface ActionRetrofitService<T> {
    void call(T p);
}
