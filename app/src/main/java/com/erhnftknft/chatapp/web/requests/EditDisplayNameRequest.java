package com.erhnftknft.chatapp.web.requests;

import com.erhnftknft.chatapp.models.ChatUser;

public class EditDisplayNameRequest {
   public ChatUser user;
   public String accessToken;
}
