package com.erhnftknft.chatapp.web.requests;

public class MessageRequest extends BaseRequest {
    public String textMessage;
    public String toUserId;
    public String imgId;
}
