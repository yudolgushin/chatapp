package com.erhnftknft.chatapp;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;

import com.erhnftknft.chatapp.db.Db;
import com.erhnftknft.chatapp.models.MessageUser;
import com.erhnftknft.chatapp.web.requests.BaseRequest;
import com.erhnftknft.chatapp.web.retrofit.RetrofitService;


public class Info {
    private static Info instance;
    private Handler handler = new Handler();

    public static synchronized Info getInstance() {
        if (instance == null) {
            instance = new Info();
        }
        return instance;
    }

    public String getAccessToken() {
        return getPrefs().getString("token", null);
    }

    public String getUserId() {
        return getPrefs().getString("userId", null);
    }

    public void setTokenAndIdAndLogin(String token, String userId, String login) {
        getPrefs().edit().putString("token", token).putString("userId", userId).putString("login", login).apply();
    }

    private SharedPreferences getPrefs() {
        return MyApp.getInstance().getSharedPreferences("default", Context.MODE_PRIVATE);
    }

    public void requestSync() {
        if (Info.getInstance().getAccessToken() == null) {
            return;
        }

        BaseRequest req = new BaseRequest();
        req.accessToken = Info.getInstance().getAccessToken();
        RetrofitService.request(a -> a.sync(req), p -> {
            for (MessageUser newMessage : p.value.newMessages) {
                Db.getInstance().saveMessage(newMessage);
            }
            requestSync();
        }, t -> {
            t.printStackTrace();
            handler.postDelayed(this::requestSync, 10_000);
        });
    }

    public String getLogin() {
        return getPrefs().getString("login", null);
    }
}
