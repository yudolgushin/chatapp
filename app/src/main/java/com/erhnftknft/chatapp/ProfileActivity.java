package com.erhnftknft.chatapp;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.erhnftknft.chatapp.messages.MessagesActivity;
import com.erhnftknft.chatapp.models.ChatUser;
import com.erhnftknft.chatapp.web.requests.GetChatUserRequest;
import com.erhnftknft.chatapp.web.retrofit.RetrofitService;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class ProfileActivity extends AppCompatActivity {
    TextView displayNameTextView;
    ChatUser user;
    private ImageView imgUser;
    private int PICK_IMAGE_REQUEST = 1;
    File file;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            Uri uri = data.getData();

            saveUriToFile(uri);

            MultipartBody.Part filePart = MultipartBody.Part.createFormData("file", file.getName(), RequestBody.create(MediaType.parse("image/*"), file));
            MultipartBody.Part accessTokenPart = MultipartBody.Part.createFormData("accessToken", Info.getInstance().getAccessToken());
            RetrofitService.request(a -> a.uploadImgUserAvatar(filePart, accessTokenPart),
                    p -> {
                        Toast.makeText(this, R.string.photo_uploaded, Toast.LENGTH_LONG).show();
                        bindViews();
                    }, t -> {
                        Toast.makeText(this, t.toString(), Toast.LENGTH_LONG).show();
                    });

        }
    }

    private void saveUriToFile(Uri uri) {
        InputStream input = null;
        try {
            input = getContentResolver().openInputStream(uri);

            file = new File(getCacheDir(), System.currentTimeMillis() + ".jpg");
            OutputStream output = new FileOutputStream(file);
            try {
                byte[] buffer = new byte[4 * 1024]; // or other buffer size
                int read;

                while ((read = input.read(buffer)) != -1) {
                    output.write(buffer, 0, read);
                }

                output.flush();
            } finally {
                output.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                input.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        Intent i = getIntent();
        user = new Gson().fromJson(i.getStringExtra("user"), ChatUser.class);
        ImageButton backButton = findViewById(R.id.back_button);
        backButton.setOnClickListener(v -> {
            onBackPressed();
        });
        TextView titleTextView = findViewById(R.id.title);
        titleTextView.setText(user.login);
        displayNameTextView = findViewById(R.id.display_name_user);
        imgUser = findViewById(R.id.user_image_view);
        ImageButton changeImgUser = findViewById(R.id.change_img_button);
        changeImgUser.setOnClickListener(v -> {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
        });
        LinearLayout ll = findViewById(R.id.change_display_name);
        ll.setOnClickListener(v -> {
            Intent intent = new Intent(ProfileActivity.this, ChangeNameActivity.class);
            intent.putExtra("user", new Gson().toJson(user));
            startActivity(intent);
            ProfileActivity.this.finish();
        });
        FloatingActionButton fab = findViewById(R.id.fab_send);
        fab.setOnClickListener(v -> {
            Intent i1 = new Intent(ProfileActivity.this, MessagesActivity.class);

            i1.putExtra("user", new Gson().toJson(user));
            startActivity(i1);
            ProfileActivity.this.finish();
        });

        bindViews();
        if (user.id.equals(Info.getInstance().getUserId())) {
            fab.setVisibility(View.INVISIBLE);
        } else {
            changeImgUser.setVisibility(View.INVISIBLE);
            ll.setClickable(false);
            if (user.displayName == null) {
                ll.setVisibility(View.INVISIBLE);
            }
            findViewById(R.id.edit_display_name_icon).setVisibility(View.INVISIBLE);
        }


    }


    

    private void bindViews() {
        GetChatUserRequest req = new GetChatUserRequest();
        req.id = user.id;
        req.accessToken = Info.getInstance().getAccessToken();
        RetrofitService.request(a -> a.getChatUserById(req),
                p -> {
                    displayNameTextView.setText(user.displayName);
                    Picasso.with(this).load(RetrofitService.BASE_URL + "/img/" + user.imgId).placeholder(R.drawable.placeholder_large).into(imgUser);
                },
                t -> {
                });


    }
}