package com.erhnftknft.chatapp.usersandchats;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.erhnftknft.chatapp.R;
import com.erhnftknft.chatapp.models.ChatUser;

import java.util.List;

public class UsersItemFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    private UsersAdapter adapter;
    UsersAndChatsActivity activity;
    private List<ChatUser> list;
    private SwipeRefreshLayout srl;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_users_item, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        srl = view.findViewById(R.id.refresh_users);
        srl.setOnRefreshListener(this);
        RecyclerView recyclerView = view.findViewById(R.id.recycler_view_users);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);

        activity = ((UsersAndChatsActivity) getActivity());
        adapter = new UsersAdapter(activity);
        recyclerView.setAdapter(adapter);


        refreshAdapter();

    }

    private void refreshAdapter() {
        if (adapter != null && list != null) {
            adapter.setItems(list);
        }
    }

    public void setList(List<ChatUser> value) {
        list = value;
        refreshAdapter();
    }

    @Override
    public void onRefresh() {
        srl.setRefreshing(true);
        activity.refreshActivity();
        srl.setRefreshing(false);
    }
}
