package com.erhnftknft.chatapp.usersandchats;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.erhnftknft.chatapp.CircleTransform;
import com.erhnftknft.chatapp.R;
import com.erhnftknft.chatapp.models.ChatHeader;
import com.erhnftknft.chatapp.web.retrofit.RetrofitService;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

class ChatsAdapter extends RecyclerView.Adapter<ChatsAdapter.ViewHolder> {
    List<ChatHeader> list = new ArrayList<>();

    UsersAndChatsActivity activity;

    public ChatsAdapter(UsersAndChatsActivity activity) {
        this.activity = activity;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.reycler_chat_item, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(list.get(position));
    }

    public void setItems(List<ChatHeader> list) {
        Collections.sort(list, (o1, o2) -> Long.compare(o2.recentMessage.messageUser.createdTs, o1.recentMessage.messageUser.createdTs));
        this.list = list;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ChatHeader header;
        TextView unreadQty;
        TextView nameUser;
        TextView lastMessageDateTextView;
        TextView recentMessage;
        ImageView iconUser;

        public ViewHolder(View itemView) {
            super(itemView);
            unreadQty = itemView.findViewById(R.id.unread_msgs_text_view);
            iconUser = itemView.findViewById(R.id.user_icon);
            nameUser = itemView.findViewById(R.id.user_name_in_chat);
            recentMessage = itemView.findViewById(R.id.last_message_user);
            lastMessageDateTextView = itemView.findViewById(R.id.last_message_date_text_view);
            itemView.setOnClickListener(v -> activity.onChatClick(header.user));
        }

        public void bind(ChatHeader chatHeader) {
            header = chatHeader;
            String qty = String.valueOf(header.recentMessage.unreadMsgs);
            if (header.recentMessage.unreadMsgs != 0) {
                unreadQty.setVisibility(View.VISIBLE);
                unreadQty.setText(qty);
            } else {
                unreadQty.setVisibility(View.INVISIBLE);
            }
            SimpleDateFormat sdf = new SimpleDateFormat("hh:mm");
            String date = sdf.format(chatHeader.recentMessage.messageUser.createdTs);
            if (header.user.displayName != null) {
                nameUser.setText(header.user.displayName);
            } else {

                nameUser.setText(chatHeader.user.login);
            }
            lastMessageDateTextView.setText(date);
            recentMessage.setText(chatHeader.recentMessage.messageUser.messageBody);

            Picasso.with(activity).load(RetrofitService.BASE_URL + "/img/" + chatHeader.user.imgId).placeholder(R.drawable.placeholder).transform(new CircleTransform()).into(iconUser);

        }
    }
}
