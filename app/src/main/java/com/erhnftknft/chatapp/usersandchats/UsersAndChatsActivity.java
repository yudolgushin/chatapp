package com.erhnftknft.chatapp.usersandchats;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.erhnftknft.chatapp.Info;
import com.erhnftknft.chatapp.LoginActivity;
import com.erhnftknft.chatapp.ProfileActivity;
import com.erhnftknft.chatapp.R;
import com.erhnftknft.chatapp.db.Db;
import com.erhnftknft.chatapp.messages.MessagesActivity;
import com.erhnftknft.chatapp.models.ChatHeader;
import com.erhnftknft.chatapp.models.ChatUser;
import com.erhnftknft.chatapp.models.MessageUser;
import com.erhnftknft.chatapp.models.MessageUserWithUnread;
import com.erhnftknft.chatapp.obs.Observer;
import com.erhnftknft.chatapp.web.requests.BaseRequest;
import com.erhnftknft.chatapp.web.requests.GetChatUserRequest;
import com.erhnftknft.chatapp.web.retrofit.RetrofitService;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class UsersAndChatsActivity extends AppCompatActivity implements Observer {
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;
    private UsersItemFragment usersItemFragment;
    private ChatsItemFragment chatsItemFragment;
    Handler handler = new Handler();
    List<ChatUser> loadedUsers = new ArrayList<>();
    private TextView title;

    @Override
    public void onBackPressed() {
        Intent startMain = new Intent(Intent.ACTION_MAIN);
        startMain.addCategory(Intent.CATEGORY_HOME);
        startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(startMain);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Db.getInstance().removeObserver(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_users_and_chats);
        Db.reset();
        Db.getInstance().addObserver(this);
        title = findViewById(R.id.title);
        title.setText(Info.getInstance().getLogin());
        ImageButton logoutButton = findViewById(R.id.logout_button);
        ImageButton editProfile = findViewById(R.id.edit_profile);
        editProfile.setOnClickListener(v -> {
            onEditProfileClick();
        });
        logoutButton.setOnClickListener(v -> {
            onExitClick();
        });

        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        Db.getInstance().getAllHeaders();
        mViewPager = findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        TabLayout tabLayout = findViewById(R.id.tabs);

        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));

        chatsItemFragment = (ChatsItemFragment) mSectionsPagerAdapter.getItem(0);
        usersItemFragment = (UsersItemFragment) mSectionsPagerAdapter.getItem(1);
        refreshActivity();

        RetrofitService.registerPushToken();
        Info.getInstance().requestSync();
    }

    private void onEditProfileClick() {
        GetChatUserRequest req = new GetChatUserRequest();
        req.id = Info.getInstance().getUserId();
        req.accessToken = Info.getInstance().getAccessToken();
        RetrofitService.request(a -> a.getChatUserById(req),
                p -> {
                    Intent i = new Intent(UsersAndChatsActivity.this, ProfileActivity.class);
                    i.putExtra("user", new Gson().toJson(p.value));
                    startActivity(i);
                }, t -> {
                });
    }

    @Override
    protected void onResume() {
        super.onResume();
        refreshActivity();
    }

    private void refreshChats() {
        if (loadedUsers == null) {
            return;
        }


        List<ChatHeader> chatHeaders = new ArrayList<>();
        for (String userId : Db.getInstance().getAllHeaders()) {
            chatHeaders.add(new ChatHeader(findUserById(userId), findRecentMsgById(userId)));
        }


        ChatsItemFragment cif = (ChatsItemFragment) mSectionsPagerAdapter.getItem(0);
        for (ChatUser chatUser : loadedUsers) {
            if (chatUser.id.equals(Info.getInstance().getUserId()))
                this.setTitle(chatUser.login);

        }

        cif.setList(chatHeaders);
    }


    private void onExitClick() {
        GetChatUserRequest req = new GetChatUserRequest();
        req.id = Info.getInstance().getUserId();
        req.accessToken = Info.getInstance().getAccessToken();
        RetrofitService.request(a -> a.logOut(req),
                p -> {
                    Info.getInstance().setTokenAndIdAndLogin(null, null, null);
                    Intent i = new Intent(UsersAndChatsActivity.this, LoginActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);
                    finish();
                },
                t -> {
                    Toast.makeText(this, t.toString(), Toast.LENGTH_LONG).show();
                });
    }

    public void refreshActivity() {
        BaseRequest r = new BaseRequest();
        r.accessToken = Info.getInstance().getAccessToken();
        RetrofitService.request(
                a -> a.getAllUsers(r),
                p -> {
                    loadedUsers = p.value;
                    UsersItemFragment uif = (UsersItemFragment) mSectionsPagerAdapter.getItem(1);
                    setTitle(findUserById(Info.getInstance().getUserId()).login);
                    loadedUsers.remove(findUserById(Info.getInstance().getUserId()));
                    uif.setList(loadedUsers);

                    refreshChats();
                },
                t -> {

                }
        );
    }

    private MessageUserWithUnread findRecentMsgById(String companionUserId) {
        List<MessageUser> msgUser = Db.getInstance().getAllMessageWithUsers(Info.getInstance().getUserId(), companionUserId);
        int unreadQty = 0;
        for (MessageUser messageUser : msgUser) {
            if (!messageUser.fromUserId.equals(Info.getInstance().getUserId())) {
                if (messageUser.alreadyRead == 0) {
                    unreadQty++;
                }
            }
        }
        return new MessageUserWithUnread(msgUser.get(msgUser.size() - 1), unreadQty);
    }

    private ChatUser findUserById(String s) {
        for (ChatUser user : loadedUsers) {
            if (user.id.equals(s)) {
                return user;
            }
        }
        int x = 10;
        return null;
    }

    public void onUserClick(ChatUser user) {
        GetChatUserRequest req = new GetChatUserRequest();
        req.id = user.id;
        req.accessToken = Info.getInstance().getAccessToken();
        RetrofitService.request(a -> a.getChatUserById(req),
                p -> {
                    Intent i = new Intent(this, ProfileActivity.class);
                    i.putExtra("user", new Gson().toJson(p.value));
                    startActivity(i);
                }, t -> {
                    Toast.makeText(UsersAndChatsActivity.this, t.toString(), Toast.LENGTH_LONG).show();
                });

    }

    public void onChatClick(ChatUser user) {
        Intent i = new Intent(this, MessagesActivity.class);
        i.putExtra("user", new Gson().toJson(user));
        startActivity(i);

    }

    @Override
    public void onMessageReceive() {
        handler.post(() ->
                refreshChats());
    }


    private class SectionsPagerAdapter extends FragmentPagerAdapter {
        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    if (chatsItemFragment == null) {
                        chatsItemFragment = new ChatsItemFragment();
                    }
                    return chatsItemFragment;

                case 1:
                    if (usersItemFragment == null) {
                        usersItemFragment = new UsersItemFragment();
                    }
                    return usersItemFragment;
            }
            return null;
        }

        @Override
        public int getCount() {
            return 2;
        }
    }
}
