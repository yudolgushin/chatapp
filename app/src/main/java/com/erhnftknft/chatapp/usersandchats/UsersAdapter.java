package com.erhnftknft.chatapp.usersandchats;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.erhnftknft.chatapp.CircleTransform;
import com.erhnftknft.chatapp.R;
import com.erhnftknft.chatapp.models.ChatUser;
import com.erhnftknft.chatapp.web.retrofit.RetrofitService;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class UsersAdapter extends RecyclerView.Adapter<UsersAdapter.ViewHolder> {
    private List<ChatUser> usersList = new ArrayList<>();

    public UsersAdapter(UsersAndChatsActivity activity) {
        this.activity = activity;
    }

    UsersAndChatsActivity activity;

    @Override
    public UsersAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_user_item, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(usersList.get(position));
    }


    public void setItems(List<ChatUser> list) {
        usersList = list;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return usersList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView loginTextView;
        ImageView iconImageView;
        ChatUser user;

        public ViewHolder(View itemView) {
            super(itemView);
            loginTextView = itemView.findViewById(R.id.user_login_or_display_name);
            iconImageView = itemView.findViewById(R.id.user_icon);
            itemView.setOnClickListener(v -> activity.onUserClick(user));
        }

        private void bind(ChatUser user) {
            this.user = user;
            if (user.displayName != null) {
                loginTextView.setText(user.displayName);
            } else {

                loginTextView.setText(user.login);
            }
            Picasso.with(activity).load(RetrofitService.BASE_URL + "/img/" + user.imgId).placeholder(R.drawable.placeholder).transform(new CircleTransform()).into(iconImageView);

        }
    }
}
