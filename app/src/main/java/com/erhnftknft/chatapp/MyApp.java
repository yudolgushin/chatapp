package com.erhnftknft.chatapp;


import android.app.Application;

import com.erhnftknft.chatapp.web.requests.BaseRequest;
import com.erhnftknft.chatapp.web.retrofit.RetrofitService;


//TODO unread counters
//TODO highlight unread chats
//TODO sync messages via HTTP long poll

public class MyApp extends Application {

    private static MyApp instance;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }

    public static MyApp getInstance() {
        return instance;
    }

}
