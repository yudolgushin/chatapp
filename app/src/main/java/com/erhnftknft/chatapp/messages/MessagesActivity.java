package com.erhnftknft.chatapp.messages;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.erhnftknft.chatapp.CircleTransform;
import com.erhnftknft.chatapp.Info;
import com.erhnftknft.chatapp.R;
import com.erhnftknft.chatapp.db.Db;
import com.erhnftknft.chatapp.models.ChatUser;
import com.erhnftknft.chatapp.models.MessageUser;
import com.erhnftknft.chatapp.obs.Observer;
import com.erhnftknft.chatapp.web.requests.MessageRequest;
import com.erhnftknft.chatapp.web.retrofit.RetrofitService;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class MessagesActivity extends AppCompatActivity implements Observer {

    private static final int PICK_IMAGE_REQUEST = 1;
    EditText messageEditText;
    ImageButton messageSendButton;
    ImageButton attFileButton;
    ChatUser user;
    private MessagesAdapter adapter;
    File file;
    String imgId;
    Handler handler = new Handler();

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Db.getInstance().removeObserver(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_messages);
        Db.getInstance().addObserver(this);
        Intent i = getIntent();
        file = null;
        user = new Gson().fromJson(i.getStringExtra("user"), ChatUser.class);
        attFileButton = findViewById(R.id.att_file_button);
        attFileButton.setOnClickListener(v -> {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
        });
        TextView textViewTitle = findViewById(R.id.title);
        if (user.displayName != null) {
            textViewTitle.setText(user.displayName);
        } else {
            textViewTitle.setText(user.login);
        }
        ImageButton backButton = findViewById(R.id.back_button);
        backButton.setOnClickListener(v -> {
            onBackPressed();
        });
        ImageView iconUser = findViewById(R.id.user_icon);
        Picasso.with(this).load(RetrofitService.BASE_URL + "/img/" + user.imgId).placeholder(R.drawable.placeholder).transform(new CircleTransform()).into(iconUser);
        messageEditText = findViewById(R.id.send_message_edit_text);
        messageSendButton = findViewById(R.id.send_message_button);
        adapter = new MessagesAdapter(this);
        RecyclerView recyclerView = findViewById(R.id.message_recycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, true));
        recyclerView.setAdapter(adapter);


        refreshAdapter();
        messageSendButton.setOnClickListener(v -> {

            if (messageEditText.getText().toString().equals("") && imgId.equals(null)) {

            } else {
                MessageRequest r = new MessageRequest();
                r.accessToken = Info.getInstance().getAccessToken();
                r.textMessage = messageEditText.getText().toString();
                r.toUserId = user.id;
                r.imgId = imgId;
                imgId = null;
                messageEditText.setText("");

                RetrofitService.request(a -> a.sendMessage(r),
                        p -> {
                            Db.getInstance().saveMessage(p.value);
                            refreshAdapter();
                        },
                        t -> {
                            Toast.makeText(this, t.toString(), Toast.LENGTH_LONG).show();
                        });
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            Uri uri = data.getData();

            saveUriToFile(uri);

            MultipartBody.Part filePart = MultipartBody.Part.createFormData("file", file.getName(), RequestBody.create(MediaType.parse("image/*"), file));
            MultipartBody.Part accessTokenPart = MultipartBody.Part.createFormData("accessToken", Info.getInstance().getAccessToken());
            RetrofitService.request(a -> a.uploadImg(filePart, accessTokenPart),
                    p -> {
                        Toast.makeText(this, R.string.photo_uploaded, Toast.LENGTH_LONG).show();
                        imgId = p.value.toString();
                        int x=0;
                    }, t -> {
                        Toast.makeText(this, t.toString(), Toast.LENGTH_LONG).show();
                    });

        }
    }

    private void saveUriToFile(Uri uri) {
        InputStream input = null;
        try {
            input = getContentResolver().openInputStream(uri);

            file = new File(getCacheDir(), System.currentTimeMillis() + ".jpg");
            OutputStream output = new FileOutputStream(file);
            try {
                byte[] buffer = new byte[4 * 1024]; // or other buffer size
                int read;

                while ((read = input.read(buffer)) != -1) {
                    output.write(buffer, 0, read);
                }

                output.flush();
            } finally {
                output.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                input.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void refreshAdapter() {
        List<MessageUser> msgs = Db.getInstance().getAllMessageWithUsers(Info.getInstance().getUserId(), user.id);
        adapter.setItems(msgs);
        Db.getInstance().setAllMessageWithUsersRead(Info.getInstance().getUserId(), user.id);

    }


    @Override
    public void onMessageReceive() {
        handler.post(() -> refreshAdapter());
    }

    public void onMessageClick(MessageUser msg) {
        AlertDialog.Builder adb = new AlertDialog.Builder(this);
        adb.setMessage("Delete this message?");
        adb.setPositiveButton("Yes", (dialog, which) -> Db.getInstance().removeMessage(msg.messageId));
        adb.setNegativeButton("No", (dialog, which) -> {

        });
        adb.show();
    }
}
