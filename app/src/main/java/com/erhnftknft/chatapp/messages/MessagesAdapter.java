package com.erhnftknft.chatapp.messages;


import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.erhnftknft.chatapp.Info;
import com.erhnftknft.chatapp.R;
import com.erhnftknft.chatapp.models.MessageUser;
import com.erhnftknft.chatapp.web.retrofit.RetrofitService;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class MessagesAdapter extends RecyclerView.Adapter<MessagesAdapter.ViewHolder> {
    private static final int INCOMING = 1;
    private static final int OUTCOMING = 2;
    List<MessageUser> list = new ArrayList<>();
    private RecyclerView recyclerView;
    MessagesActivity activity;

    public MessagesAdapter(MessagesActivity activity) {
        this.activity = activity;
        setHasStableIds(true);
    }

    @Override
    public MessagesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        int viewResId = viewType == INCOMING ? R.layout.recycler_message_item_incoming : R.layout.recycler_message_item_outcoming;
        View itemView = LayoutInflater.from(parent.getContext()).inflate(viewResId, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MessagesAdapter.ViewHolder holder, int position) {
        holder.bind(list.get(position));
    }

    @Override
    public long getItemId(int position) {
        return list.get(position).messageId.hashCode();
    }

    @Override
    public int getItemViewType(int position) {

        if (list.get(position).fromUserId.equals(Info.getInstance().getUserId())) {
            return OUTCOMING;
        }
        return INCOMING;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void setItems(List<MessageUser> list) {
        this.list = list;
        Collections.sort(list, (o2, o1) -> Long.compare(o1.createdTs, o2.createdTs));
        notifyDataSetChanged();

        if (!list.isEmpty()) {
            recyclerView.smoothScrollToPosition(0);
        }
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        this.recyclerView = recyclerView;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView messageBodyTextView;
        TextView dateTextView;
        ImageView msgImg;
        MessageUser msg;

        public ViewHolder(View itemView) {
            super(itemView);
            messageBodyTextView = itemView.findViewById(R.id.message_body_text_view);
            dateTextView = itemView.findViewById(R.id.date_text_view);
            msgImg = itemView.findViewById(R.id.msg_img);
            itemView.setOnLongClickListener(v -> {
                activity.onMessageClick(msg);
                return true;
            });
        }

        public void bind(MessageUser messageUser) {
            msg = messageUser;
            if (msg.imgId == null) {
                msgImg.setVisibility(View.GONE);

            } else {
                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                lp.setMargins(0, msgImg.getHeight() + 4, 30, 0);
                messageBodyTextView.setLayoutParams(lp);

                msgImg.setVisibility(View.VISIBLE);
                Picasso.with(activity).load(RetrofitService.BASE_URL + "/img/" + msg.imgId).placeholder(R.drawable.placeholder_large).into(msgImg);

            }
            if (messageUser.fromUserId.equals(Info.getInstance().getUserId())) {
                messageBodyTextView.setGravity(Gravity.RIGHT);
            }
            messageBodyTextView.setText(messageUser.messageBody);
            long dateInMs = messageUser.createdTs;
            SimpleDateFormat sdf = new SimpleDateFormat("hh:mm");
            dateTextView.setText(sdf.format(new Date(dateInMs)));

        }
    }
}
