package com.erhnftknft.chatapp.models;


public class ChatHeader {
    public ChatUser user;
    public MessageUserWithUnread recentMessage;

    public ChatHeader(ChatUser user, MessageUserWithUnread recentMessage) {
        this.user = user;
        this.recentMessage = recentMessage;
    }
}
