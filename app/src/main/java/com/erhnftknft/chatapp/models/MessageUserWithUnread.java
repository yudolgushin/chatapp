package com.erhnftknft.chatapp.models;


public class MessageUserWithUnread {
    public MessageUser messageUser;
    public int unreadMsgs;

    public MessageUserWithUnread(MessageUser messageUser, int unreadMsgs) {
        this.messageUser = messageUser;
        this.unreadMsgs = unreadMsgs;
    }
}
