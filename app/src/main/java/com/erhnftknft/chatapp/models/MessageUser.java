package com.erhnftknft.chatapp.models;


public class MessageUser {
    public String fromUserId;
    public String toUserId;
    public String messageBody;
    public long createdTs;
    public String messageId;
    public int alreadyRead;
    public String imgId;

    public MessageUser(String fromUserId, String toUserId, String messageBody, long createdTs, String messageId, int alreadyRead) {
        this.fromUserId = fromUserId;
        this.toUserId = toUserId;
        this.messageBody = messageBody;
        this.createdTs = createdTs;
        this.messageId = messageId;
        this.alreadyRead = alreadyRead;
    }
}
