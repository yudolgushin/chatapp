package com.erhnftknft.chatapp.db;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.erhnftknft.chatapp.Info;

public class DBHelper extends SQLiteOpenHelper {
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "my_messages_";

    public static final String MESSAGE_TABLE_NAME = "messages";

    public static final String MESSAGE_TO_USER_ID = "to_user_id";
    public static final String MESSAGE_FROM_USER_ID = "from_user_id";
    public static final String MESSAGE_BODY = "message_body";
    public static final String MESSAGE_CREATED_TS = "created_ts";
    public static final String MESSAGE_ID = "message_id";
    public static final String MESSAGE_READ = "read";
    private static final String LOG_TAG = "DBHelper";
    public static final String MESSAGE_ID_IMG = "img_id";


    public DBHelper(Context context) {
        super(context, DATABASE_NAME + Info.getInstance().getUserId(), null, DATABASE_VERSION);
        Log.d(LOG_TAG, "DB_NAME -" + getDatabaseName());
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("create table if not exists " + MESSAGE_TABLE_NAME +
                "(" + MESSAGE_FROM_USER_ID + " text ," +
                MESSAGE_TO_USER_ID + " text, "
                + MESSAGE_BODY + " text, "
                + MESSAGE_CREATED_TS + "  integer, "
                + MESSAGE_ID + " text, "
                + MESSAGE_READ + " integer,"
                + MESSAGE_ID_IMG + " text);");

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
    }
}