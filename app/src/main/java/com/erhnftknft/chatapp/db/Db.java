package com.erhnftknft.chatapp.db;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.erhnftknft.chatapp.Info;
import com.erhnftknft.chatapp.MyApp;
import com.erhnftknft.chatapp.obs.Observble;
import com.erhnftknft.chatapp.obs.Observer;
import com.erhnftknft.chatapp.models.MessageUser;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Db implements Observble {
    List<Observer> observers = new ArrayList<>();
    private static final String LOG_TAG = "DB_LOG";
    private final SQLiteDatabase database;
    private static Db instance;

    private Db() {
        DBHelper dbHelper = new DBHelper(MyApp.getInstance());
        database = dbHelper.getWritableDatabase();
    }


    public static Db getInstance() {
        if (instance == null) {
            instance = new Db();
        }
        return instance;
    }

    public void saveMessage(MessageUser message) {
        Object[] bindArgs = {message.fromUserId, message.toUserId, message.messageBody, String.valueOf(message.createdTs), message.messageId, message.alreadyRead, message.imgId};
        database.execSQL("insert into " + DBHelper.MESSAGE_TABLE_NAME + " " +
                "(" + DBHelper.MESSAGE_FROM_USER_ID + ", "
                + DBHelper.MESSAGE_TO_USER_ID + ", "
                + DBHelper.MESSAGE_BODY + ", "
                + DBHelper.MESSAGE_CREATED_TS + ","
                + DBHelper.MESSAGE_ID + ", "
                + DBHelper.MESSAGE_READ + ","
                + DBHelper.MESSAGE_ID_IMG + " )" +
                " values " +
                "(?,?,?,?,?,?,?)", bindArgs);


        Log.d(LOG_TAG, "Save message " + message);
        notifyObserver();
    }

    public List<MessageUser> getAllMessageWithUsers(String currentUserId, String companionUserId) {

        List<MessageUser> list = new ArrayList<>();
        String[] values = {currentUserId, companionUserId, companionUserId, currentUserId};

        Cursor c = database.rawQuery("select * from " + DBHelper.MESSAGE_TABLE_NAME +
                        " where (to_user_id = ? and from_user_id = ? ) or (to_user_id = ? and from_user_id = ?);",
                values);
        if (c.getCount() > 0) {
            while (c.moveToNext()) {
                list.add(new MessageUser(c.getString(0), c.getString(1), c.getString(2), c.getLong(3), c.getString(4), c.getInt(5)));
                list.get(list.size() - 1).imgId = c.getString(6);
            }
        }
        c.close();
        return list;
    }

    @Override
    public void addObserver(Observer observer) {
        observers.add(observer);
    }

    @Override
    public void removeObserver(Observer observer) {
        observers.remove(observer);
    }

    @Override
    public void notifyObserver() {
        for (Observer observer : observers) {
            observer.onMessageReceive();
        }
    }

    public Set<String> getAllHeaders() {     //TODO use one query
        Set<String> ids = new HashSet<>();
        Cursor c = database.rawQuery("select distinct " + DBHelper.MESSAGE_FROM_USER_ID + " from " + DBHelper.MESSAGE_TABLE_NAME + " ", null);
        if (c.getCount() > 0) {
            while (c.moveToNext()) {
                ids.add(c.getString(0));
            }
        }
        c.close();

        c = database.rawQuery("select distinct " + DBHelper.MESSAGE_TO_USER_ID + " from " + DBHelper.MESSAGE_TABLE_NAME, null);
        if (c.getCount() > 0) {
            while (c.moveToNext()) {
                ids.add(c.getString(0));
            }
        }
        c.close();
        ids.remove(Info.getInstance().getUserId());
        return ids;
    }

    public void removeMessage(String messageId) {
        String[] bindArgs = {messageId};
        database.execSQL("delete  from " + DBHelper.MESSAGE_TABLE_NAME + " where message_id =?", bindArgs);
        notifyObserver();
    }

    public static void reset() {
        instance = null;
    }

    public void setAllMessageWithUsersRead(String myUserId, String companionUserId) {
        String bindArgs[] = {companionUserId, myUserId};
        database.execSQL("update " + DBHelper.MESSAGE_TABLE_NAME + " set " + DBHelper.MESSAGE_READ + "= 1 where " + DBHelper.MESSAGE_FROM_USER_ID + "=? and " + DBHelper.MESSAGE_TO_USER_ID + "=?", bindArgs);

    }
}