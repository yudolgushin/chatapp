package com.erhnftknft.chatapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;
import android.widget.Toast;

import com.erhnftknft.chatapp.models.ChatUser;
import com.erhnftknft.chatapp.web.requests.EditDisplayNameRequest;
import com.erhnftknft.chatapp.web.retrofit.RetrofitService;
import com.google.gson.Gson;

public class ChangeNameActivity extends AppCompatActivity {

    private ChatUser user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_name);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        Intent i = getIntent();
        user = new Gson().fromJson(i.getStringExtra("user"), ChatUser.class);

        FloatingActionButton fab = findViewById(R.id.fab);
        EditText displayNameChangeEditText = findViewById(R.id.display_name_edit_text);
        if (user.displayName != null) {
            displayNameChangeEditText.setText(user.displayName + " ");
        }

        fab.setOnClickListener(v -> {
            String newDisplayNameString = user.displayName = displayNameChangeEditText.getText().toString();
            if (haveSpace(newDisplayNameString) || veryLong(newDisplayNameString)) {
                Toast.makeText(ChangeNameActivity.this, R.string.incorrect_value, Toast.LENGTH_LONG).show();
                return;
            }
            EditDisplayNameRequest req = new EditDisplayNameRequest();
            req.accessToken = Info.getInstance().getAccessToken();
            req.user = user;
            RetrofitService.request(a -> a.refreshDisplayNameUser(req),
                    p -> {
                        Toast.makeText(ChangeNameActivity.this, "Name has been changed", Toast.LENGTH_LONG).show();
                        onBackPressed();
                    }, t -> {
                        Toast.makeText(ChangeNameActivity.this, t.toString(), Toast.LENGTH_LONG).show();
                    });
            Intent intent = new Intent(ChangeNameActivity.this, ProfileActivity.class);
            intent.putExtra("user", new Gson().toJson(user));
            startActivity(intent);
        });
    }

    private boolean haveSpace(String newDisplayNameString) {
        if (newDisplayNameString.contains(" ")) {
            return true;
        }
        return false;
    }

    private boolean veryLong(String newDisplayNameString) {
        if (newDisplayNameString.length() > 25) {
            return true;
        }
        return false;
    }

    @Override
    public boolean onSupportNavigateUp() {
        Intent i = new Intent(this, ProfileActivity.class);
        i.putExtra("user", new Gson().toJson(user));
        startActivity(i);
        return true;
    }
}
